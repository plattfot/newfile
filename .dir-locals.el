;;; Directory Local Variables
;;; See Info node `(emacs) Directory Variables' for more information.

;; ((nil . ((newfile-project-name . "newfile"))))
;; Fetch the name from the directory it's in
((nil . ((eval . (setq newfile-project-name (progn (let ( (path (file-name-directory (file-truename (dir-locals-find-file ".")))) )
						     (string-match ".*/\\(.*\\)/" path)
						     (match-string-no-properties 1 path)
						     ))))
	 ))
)
