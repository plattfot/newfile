;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Copyright (c) 2014 Mihai Alden and Fredrik Salomonsson
;;
;; All rights reserved. This software is distributed under the
;; GNU Lesser General Public License (https://www.gnu.org/copyleft/lgpl)
;;
;; Redistributions of source code must retain the above copyright
;; and license notice.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Add (require 'newfile) to your emacs file and you're good to go.
;; Also you'll need to set newfile-project-name to the project name.
;; One thing you could do is add ((nil . ((newfile-project-name
;; . "newfile")))) to the .dir-locals.el in root of the project

;; Todo change author to be based on username or something.

;; ========================== Global variables =================================
(defvar newfile-project-name "") ;; name of the project, i.e the base namespace
(defvar newfile-boilerplate "") ;; path to the boilerplate default is the root of the project

;; ============================ Project Path ===================================
(defun newfile/path ()
  "Returns the path to the project, must be called inside the nerv project to work"
  (interactive)
  (let ((path (pwd)))
    (string-match newfile-project-name path)
    ;; Remove everything after project name
    (substring path 10 (match-end 0)))
  )

(defun newfile/is-header (extension)
  "Returns t if the file is a header file"
  (interactive)
  (string-match "^h" extension)
  )

;; ========================= Comment character =================================
(defun newfile/comment-char (extension)
  (cond ((string= "el" extension) ";;")
	((string= "py" extension) "#")
	((string= "hs" extension) "--")
	((string-match "^[ch]\\(pp\\|\\|c\\|$\\)" extension) "//") 
	(t "#")
	)
  )

;; ============================ Boilerplate ====================================
(defun newfile/insert-boilerplate ()
  "Insert boilerplate. Reads from file to avoid copyright issues. COMMENT_CHAR specify the char to wrap the boilerplat default to //"
  (interactive)
  (let ((boilerplate (get-string-from-file (if (string= "" newfile-boilerplate)
					       (concat (newfile/path) "/boilerplate")
					     newfile-boilerplate)))
	(pos (point))
	(comment (newfile/comment-char (file-name-extension (buffer-name))))
	multiple
	(start 0))

    (setq multiple (if (equal (length comment) 1) 2 1))
    (setq boilerplate (replace-regexp-in-string "\\[::date::\\]" (format-time-string "%Y") boilerplate))

    (while (string-match "{comment,\\([0-9]+\\)}" boilerplate start )
      (setq start (match-end 0))
      (let ((nr (string-to-number (match-string 1 boilerplate)))
	    (nr_of_comments )	    
	    (comment_text "")
	    (count 0))
	(setq nr_of_comments (* multiple nr)) 
	(while (< count nr_of_comments)
	  (setq comment_text (concat comment_text comment))
	  (setq count (1+ count)))
	(setq boilerplate (replace-regexp-in-string (concat "{comment,"(number-to-string nr)"}") comment_text boilerplate))
	))
    (insert boilerplate))
  )

;; ============================ Description ====================================
(defun newfile/insert-description ()
  "Insert description of when the file was created who's the author
and the name of the file."
  (interactive)
  (insert (mapconcat 'identity 
		     ( list "/// @author Fredrik Salomonsson, Mihai Alden "
			    "///"
			    ( concat "/// @date   " (format-time-string "%B %Y") )) 
		     "\n") )
  )
;; ============================== Header =======================================
(defun newfile/insert-header()
  "Insert boilerplate and descrition"
  (interactive)
  (newfile/insert-boilerplate)
  (newline)
  (forward-line 1)
  (newfile/insert-description)
  )

;; ============================== Namespace ====================================
(defvar newfile-include-gaurd nil)

(defun newfile/insert-incl-guard (path_list)
  "Insert include guard based on the path-list"
  (setq newfile-include-guard (concat (mapconcat 'upcase path_list "_")
				      "_" 
				      (upcase 
				       (underscore 
					(camelcase (file-name-sans-extension (buffer-name)))
					)
				       )
				      "_HAS_BEEN_INCLUDED"
				      )
	)
  ;; Insert all into buffer
  (insert "#ifndef " newfile-include-guard "\n" "#define " newfile-include-guard "\n\n")
  )

(defun newfile/insert-namespace ( extra_namespaces )
  "Insert namespace and include guard based on the location on the file"
  (interactive "sAdd extra namespace (separated by space): ")
  (let ( ;; Declare variables
	(path (pwd))
	start 
	end
	path_list
	extra_list
	include_guard)
    (setq start (string-match newfile-project-name path))
    (setq end (- (length path) 1))
    ;; Remove everything until project name
    (setq path (substring path start end) )
    ;; Split it into a list
    (setq path_list ( split-string  path "/") )

    ;; Add extra path_list
    (setq extra_list (split-string extra_namespaces ) )
    (setq path_list (append path_list extra_list))
    ;; Create include guard name
    (let ((is_header (newfile/is-header (file-name-extension (buffer-name)))))
      (if is_header
	  (newfile/insert-incl-guard path_list)
	nil)
      (dolist (x path_list) (insert "namespace " x " {\n} // namespace " x "\n")
	      (search-backward "{")
	      (forward-char 2))
      (insert "\n")
      (forward-line (length path_list))
      (if is_header
	  (insert "\n#endif // " newfile-include-guard)
	nil)
      
      (forward-line (- (+ (length path_list) (if is_header 2 1)) )))
    ))

(defun newfile/create (args)
  "Adds boilerplate, description and namespaces"
  (interactive "sAdd extra namespace (separated by space): ")

  (if (string= "" newfile-project-name)
      (message "Need to set the newfile-project-name to the name of the project")
    (progn (newfile/insert-header)
	   (insert "\n\n")
	   (newfile/insert-namespace args)
	   ))
  )

(provide 'newfile)

