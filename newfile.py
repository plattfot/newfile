#! /usr/bin/env python2
# Change the above to your python path, I'm using arch linux which
# the default is python 3 hence the 2 after python to use python 2.x - Fredrik
############################################################################
##
## Copyright (c) 2014 Mihai Alden and Fredrik Salomonsson
##
## All rights reserved. This software is distributed under the
## GNU Lesser General Public License (https://www.gnu.org/copyleft/lgpl)
##
## Redistributions of source code must retain the above copyright
## and license notice.
##
############################################################################

__doc__ = """Insert boilerplate, namespaces based on file location and include guards if the file is a header, i.e file with the extension h or hpp.
"""
import sys
import os
import argparse
import re
import time

def commentChar( extension ):
    if extension == ".el":
        return ";;"
    elif extension == ".py":
        return "#"
    elif extension == ".hs":
        return "--"
    elif re.match("\.[ch](pp|$)", extension ):
        return "//"
    else :
        return "#"

def doxyComment( extension ):
    if extension == ".el":
        return ";;"
    elif extension == ".py":
        return "##"
    elif extension == ".hs":
        return "--"
    elif re.match("\.[ch](pp|$)", extension ):
        return "///"
    else :
        return "#"
    
# end commentChar
def projectPath( project ):
    path = os.getcwd()
    match = re.search(".*/{0}".format( project ), path )
    return match.group( 0 )
    
def getBoilerplate( comment_char, project, path='' ):
    if path:
        filename = path
    else:
        filename = os.getenv('NEWFILE_BOILERPLATE')
        if not filename:
            filename = projectPath( project ) + "/boilerplate" 

    with open( filename,"r") as b_file:
        # Read the whole file
        boilerplate = b_file.read()
    b_file.close()

    if( len(comment_char) == 1 ):
        mult = 2
    else:
        mult = 1
    
    pattern = re.compile("\{comment,([0-9]+)\}")
    start = 0
    while True:
        match = pattern.search( boilerplate, start )
        if match:
            start = match.end( 0 )
            nr = int( match.group( 1 ) )
            nr_of_comments = nr * mult
            comment = ""
            for x in xrange(nr_of_comments):
                comment += comment_char
            boilerplate = re.sub("\{comment,%d\}"%nr, comment, boilerplate )
        else :
            break
    boilerplate = re.sub("\[::date::\]", time.strftime("%Y"), boilerplate );

    return boilerplate
# end getBoilerplate
def getDescription( extension ):
    doxy_comment = doxyComment( extension )
    if os.environ['USER'] == 'plattfot':
        author = " @author Fredrik Salomonsson, Mihai Alden"
    else:
        author = " @author Mihai Alden, Fredrik Salomonsson"
    return doxy_comment + author + "\n" + doxy_comment + "\n" + doxy_comment + " @date   "+time.strftime("%B %Y")
    
def getNamespaces( path, project, extra ):
    if re.search("^/", path ):
        # path is an absolute path
        abspath = path
    else:
        # get absolute path
        abspath = os.path.abspath( path )
    head, tail = os.path.split( abspath )
    match = re.search( "{0}.*".format(project), head )
    path_from_project = match.group( 0 )
    namespaces = path_from_project.split("/")
    if extra:
        namespaces += extra
    return  namespaces

# end getNamespaces
    
def makeIncludeGuardFrom( namespaces, filename ):
    include_guard = '_'.join( namespaces )
    include_guard += '_' + filename
    include_guard += "_has_been_included"
    include_guard = include_guard.upper()
    return include_guard
# end makeIncludeGuardFrom
def setupNewFile( args ):
    
    path, extension = os.path.splitext(args.filename)
    null, filename = os.path.split( path )

    if( args.extension ):
        extension = "." + args.extension

    project = args.project
    # if no project name has been specified check the environment variable
    if not project:
        project = os.getenv('NEWFILE_PROJECT_NAME')
        if not project:
            print "Need to specify the project name either passing it in via -p or setting "\
                  "the environment variable NEWFILE_PROJECT_NAME"
            sys.exit()
    
    comment_char = commentChar( extension )

    boilerplate = getBoilerplate( comment_char, project )
    
    namespaces = getNamespaces( args.filename, project, extra=args.namespace )

    include_guard = makeIncludeGuardFrom(namespaces, filename )
    description = getDescription( extension )

    
    head  = boilerplate + "\n"
    head += description + "\n\n"
    tail = ""
    match = re.match("\.([ch])(pp|$)", extension )
    if( match ):
        ext = match.group(1)
        # if it's a header file
        if(ext == "h"):
            head += "#ifndef "+include_guard + "\n"
            head += "#define "+include_guard + "\n"
        
        tail = "\n"
        
        for n in namespaces:
            head += "namespace "+n+" {\n"
        
        for n in reversed(namespaces):
            tail += "} // " + n + "\n"
        if(ext == "h"):
            tail += "#endif // " + include_guard
    if(args.stdout):
        print head, tail
    else:
        # Create if not exist and read its content
        with open(args.filename,'a+') as new_file:
            new_file.seek(0)
            current_content = new_file.read()
        new_file.close()
        # Open it for writing and overwrite its content.
        with open(args.filename, 'w+') as new_file:
            new_file.write( head + '\n' + current_content + tail )
        new_file.close()
# end setupNewFile    
# Based on Guido van Rossu's main function
class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def process(arg):
    print arg

def main(argv=None):

    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description = "Setup new file: Simple program that inserts the boilerplate, "
                                     "add correct namespaces and if the file is a header adds include guards "
                                     "based on namespaces and filename.")
    parser.add_argument('filename',
                        help="filename in which to setup, if it doesn't exist it will be created.")
    parser.add_argument("-n", "--namespace",nargs='*', dest='namespace', type=str,
                        help="Add extra namespaces, for example usage \"-n foo bar\" will add foo and bar as extra namespaces")
    parser.add_argument("-e", "--extension",nargs='?', dest='extension',
                        help="Override the file extension, default is to check the extension "
                        "and if no extension exist it will fallback to #")
    parser.add_argument("-o", "--stdout",action="store_true", dest='stdout', default=False,
                        help="Output to stdout instead of the file")
    parser.add_argument("-p","--project",nargs='?', dest='project', type=str,
                        help="Name of the project, i.e the base namespace it will use. "
                        "This can also be set with the environment variable NEWFILE_PROJECT_NAME")
    parser.add_argument("-b","--boilerplate",nargs='?', dest='boilerplate', type=str,
                        help="Path to the boilerplate including the filename of the boilerplate. "
                        "This can also be set by the environment variable NEWFILE_BOILERPLATE. "
                        "Default is that it looks for a file called boilerplate in the root of the project.")

    setupNewFile( parser.parse_args() )

    return 0
    
if __name__ == "__main__":
    sys.exit(main())


